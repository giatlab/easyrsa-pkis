undefined() {
    [ z"${!1}" == z ]
}

set_undefined() {
    printf -v "$1" "${!1:-$2}"
}

die() {
    local code=$1
    shift
    >&2 echo $@
    exit $code
}

assert_non_empty() {
    if [ z"$2" == z ]; then
        echo $1
        exit 1
    fi
}

assert_var_non_empty() {
    assert_non_empty "$1 is empty" "${!1}"
}

exit_undefined_usage() {
    if undefined $1; then
        >&2 echo Environment variable $1 is needed
        >&2 echo
        usage
        exit 1
    fi
}

LOG() {
    echo '------' $@
}

_mkcd() {
    mkdir -p $1
    cd $1
}

_rmmkcd() {
    rm -rf $1
    mkdir -p $1
    cd $1
}

_touchwith() {
    if [ $# -eq 0 ]; then
        return 1
    fi
    local file=$1
    shift
    if [ ! -e $file ]; then
        if [ $# -eq 0 ]; then
            cat >$file
        else
            echo $@ >$file
        fi
    fi
}

_list_d() {
    if [ $# -ne 1 ]; then
        echo "usage: _list_d list_name"
        return 1
    fi
    local l=$1
    if [ -r $l.list ]; then
        echo $l.list
    fi
    if [ -d $l.list.d ]; then
        for s in $l.list.d/*.list ; do
            if [ -r $s ]; then
                echo $s
            fi
        done
    fi
}

read_into_vars_assert() {
    if [ $# -lt 1 ]; then
        echo "usage: read_into_vars var1 var2 <<<$line"
        return 1
    fi
    read $@ __remain <<<$(sed -e 's/#.*$//')
    for v in $@; do
        assert_var_non_empty $v
    done
}
