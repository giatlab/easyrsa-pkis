#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1

root_ca=$dir/root-ca

gen_sub_ca_list() {
    local list=$1
    local init_script=$2
    local ca_type=$3

    touch $list
    while read -u 3 -r line; do
        read_into_vars_assert subca_dir subca_cn <<<"$line"
        subca_dir=$dir/$subca_dir
        LOG sub-ca $subca_cn
        $init_script $subca_dir
        echo "$subca_cn" >$subca_dir/$ca_type.txt
        build_ca $subca_dir/$ca_type "$subca_cn" subca nopass
        import_sign_subca $root_ca $subca_dir/$ca_type $subca_cn
        ca_crt=$subca_dir/$ca_type/pki/ca.crt
        crt_subject $ca_crt
        LOG sub-ca $subca_cn finished
    done 3< <(grep -v '^\s*#' $list)
}

for l in $(_list_d $dir/sub-norm-ca); do
    gen_sub_ca_list $l $__DIR__/init-norm-ca.sh ca
done
for l in $(_list_d $dir/sub-root-ca); do
    gen_sub_ca_list $l $__DIR__/init-root-ca.sh root-ca
done
