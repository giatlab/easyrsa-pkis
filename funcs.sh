#!/bin/bash

__TMPLS__=$__DIR__/templates

init_pki() {
    if [ $# -ne 1 ]; then
        echo "usage: init_pki dir"
        return 1
    fi
    local dir=$1
    mkdir -p $dir

    local pki_dir=$dir/pki
    if [[ -d $pki_dir ]]; then
        echo "$pki_dir exists"
        return
    fi

    local links="openssl-easyrsa.cnf x509-types"
    for p in $links; do
        if [[ ! -e $p ]]; then
            cp -r $__TMPLS__/$p $dir
        fi
    done

    EASYRSA=$dir easyrsa --batch init-pki
}

build_ca() {
    if [ $# -lt 1 ]; then
        echo "usage: build_ca dir opts..."
        return 1
    fi
    local dir=$1
    local cn="$2"
    shift 2

    local ca_key=$dir/pki/private/ca.key
    if [ ! -e $ca_key ]; then
        EASYRSA=$dir easyrsa --batch --dn-mode=cn_only --req-cn="$cn" build-ca $@
    fi
}

gen_req() {
    if [ $# -ne 2 ]; then
        echo "usage: gen_req dir req_name"
        return 1
    fi
    local dir=$1
    local server=$2
    local priv_key=$dir/pki/private/$server.key
    if [[ ! -f $priv_key ]]; then
        EASYRSA=$dir easyrsa --batch --dn-mode=cn_only --req-cn=$server gen-req $server nopass
    fi
}

import_req() {
    if [ $# -ne 3 ]; then
        echo "usage: import_req ca_dir req_path req_name"
        return 1
    fi
    local dir=$1
    local req=$2
    local name=$3

    local req_key=$dir/pki/reqs/$name.req
    if [ ! -e $req_key ]; then
        EASYRSA=$dir easyrsa --batch import-req $req $name
    fi
}

sign_req() {
    if [ $# -ne 3 ]; then
        echo "usage: sign_req ca_dir type req_name"
        return 1
    fi
    local dir=$1
    local type=$2
    local name=$3

    local crt=$dir/pki/issued/$name.crt
    if [ ! -e $crt ]; then
        EASYRSA=$dir easyrsa --batch sign-req $type $name
    fi
}

import_sign_subca() {
    if [ $# -ne 3 ]; then
        echo "usage: import_sign_req root_ca_dir subca_dir subca_cn"
        return 1
    fi
    local root_ca_dir=$1
    local subca_dir=$2
    local subca_cn=$3
    local ca_crt=$subca_dir/pki/ca.crt
    if [ ! -r $ca_crt ]; then
        local req=$subca_dir/pki/reqs/ca.req
        if [ ! -r $req ]; then
            echo "$subca_dir: neither $ca_crt nor $req found!"
            exit 1
        fi
        import_req $root_ca_dir $req $subca_cn
        sign_req $root_ca_dir ca $subca_cn
        cp $root_ca_dir/pki/issued/$subca_cn.crt $ca_crt
    fi
}

crt_subject() {
    openssl x509 -in $1 -subject -noout
}

gen_dh() {
    if [ $# -ne 1 ]; then
        echo "usage: gen_dh file"
        return 1
    fi
    local dh=$1
    if [[ ! -f $dh ]]; then
        openssl dhparam -out $dh 2048
    fi
}

gen_pkcs12() {
    if [ $# -ne 3 ]; then
        echo "usage: gen_pkcs12 ca_dir entity_dir name"
        return 1
    fi
    local ca_dir=$1
    local entity_dir=$2
    local name=$3

    local p12=$entity_dir/pki/private/$name.p12
    local entity_crt=$ca_dir/pki/issued/$name.crt
    local priv_key=$entity_dir/pki/private/$name.key
    local ca_crt=$ca_dir/pki/ca.crt
    if [[ ! -f $p12 ]]; then
        openssl pkcs12 -in $entity_crt -inkey $priv_key -certfile $ca_crt -export -out $p12
    fi
}
