#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1
name=$(basename $dir)

init_pki $dir/ca
init_pki $dir/server
init_pki $dir/client
_touchwith $dir/ca.txt "# ca@$name"
_touchwith $dir/server.list "# server@$name"
_touchwith $dir/client.list "# client@$name"
