#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1
name=$(basename $dir)

init_pki $dir/root-ca
_touchwith $dir/root-ca.txt "# root-ca@$name"
_touchwith $dir/sub-root-ca.list "# dir root-ca@sub.$name"
_touchwith $dir/sub-norm-ca.list "# dir ca@sub.$name"
