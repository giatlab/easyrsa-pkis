#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1

ca_txt=$dir/ca.txt
if [ ! -r $ca_txt ]; then
    echo "$ca_txt not readable"
    exit 1
fi
read_into_vars_assert cn <<<$(grep -v '^\s*#' $ca_txt)

LOG ca $cn
ca_crt=$dir/ca/pki/ca.crt
build_ca $dir/ca "$cn"
crt_subject $ca_crt
LOG ca $cn finished
