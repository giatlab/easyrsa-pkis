#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1

root_ca_txt=$dir/root-ca.txt
if [ ! -r $root_ca_txt ]; then
    echo "$root_ca_txt not readable"
    exit 1
fi
read_into_vars_assert cn <<<$(grep -v '^\s*#' $root_ca_txt)

LOG root-ca $cn
ca_crt=$dir/root-ca/pki/ca.crt
build_ca $dir/root-ca "$cn"
crt_subject $ca_crt
LOG root-ca $cn finished
