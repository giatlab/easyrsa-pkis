#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1

if [ ! -d $dir ]; then
    echo "$dir not a directory"
    exit 1
fi

dh_dir=$dir/server/dh
mkdir -p $dh_dir

for l in $(_list_d $dir/server); do
    while read -u 3 -r line; do
        read_into_vars_assert server <<<"$line"

        LOG server $server
        gen_dh $dh_dir/$server-dh.pem

        gen_req $dir/server $server
        import_req $dir/ca $dir/server/pki/reqs/$server.req $server
        sign_req $dir/ca server $server
        LOG server $server finished
    done 3< <(grep -v '^\s*#' $l)
done
