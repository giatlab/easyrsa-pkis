#!/bin/bash

set -e

trap '>&2 echo Error on line $LINENO' ERR

if [ $# -ne 1 ]; then
    echo "usage: $0 dir"
    exit 1
fi

__DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. $__DIR__/common.sh
. $__DIR__/funcs.sh

dir=$1

if [ ! -d $dir ]; then
    echo "$dir not a directory"
    exit 1
fi

for l in $(_list_d $dir/client); do
    while read -u 3 -r line; do
        read_into_vars_assert client <<<"$line"

        LOG client $client

        gen_req $dir/client $client
        import_req $dir/ca $dir/client/pki/reqs/$client.req $client
        sign_req $dir/ca client $client

        gen_pkcs12 $dir/ca $dir/client $client

        LOG client $client finished
    done 3< <(grep -v '^\s*#' $l)
done
